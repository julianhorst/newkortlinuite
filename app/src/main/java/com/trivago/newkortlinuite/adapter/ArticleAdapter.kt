package com.trivago.newkortlinuite.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trivago.newkortlinuite.R
import com.trivago.newkortlinuite.models.Article
import kotlinx.android.synthetic.main.item_article.view.*

class ArticleAdapter(val articleList: List<Article>) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    // Adapter

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ArticleAdapter.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_article, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindArticles(articleList[position])
    }

    override fun getItemCount() = articleList.size


    // ViewHolder

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindArticles(article: Article) {
            itemView.title.text = article.title
            itemView.author.text = article.author
            itemView.abstractText.text = article.abstract
        }
    }
}