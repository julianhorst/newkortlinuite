package com.trivago.newkortlinuite.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class Article(
        @SerializedName("title") val title: String,
        @SerializedName("byline") val author: String,
        @SerializedName("abstract") val abstract: String,
        @SerializedName("url") val url: String,
        var date: Date
) {
}
