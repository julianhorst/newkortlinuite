package com.trivago.newkortlinuite.network

import android.util.Log
import com.google.gson.GsonBuilder
import com.trivago.newkortlinuite.models.Article
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RestClient(val onResponseListener: OnResponseListener,
                 val timeSpan: String = "30") : Callback<List<Article>> {


    // Members

    private val BASE_URL = "http://api.nytimes.com"

    // Constructor

    fun start() {
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .registerTypeAdapter(List::class.java, ArticleDeserializer())
                .create()

        val restAdapter = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        val searchApi = restAdapter.create<SearchApi>(SearchApi::class.java)

        val call = searchApi.getArticles("all-sections", timeSpan, "92a65fab33c54092a76311297875d883")
        call.enqueue(this)
    }

    override fun onFailure(call: Call<List<Article>>?, t: Throwable?) {
        Log.d("ttt", t.toString())
    }

    override fun onResponse(call: Call<List<Article>>?, response: Response<List<Article>>?) {
        Log.d("ttt", "jaaaa")
        onResponseListener.onResponse(response?.body())

    }
}

interface OnResponseListener {
    fun onResponse(articleList: List<Article>?)
}
