package com.trivago.newkortlinuite.network

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.trivago.newkortlinuite.models.Article
import java.lang.reflect.Type
import java.text.SimpleDateFormat


class ArticleDeserializer : JsonDeserializer<List<Article>> {

    // JsonDeserializer
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): List<Article> {

        val jsonResultsArray = json.asJsonObject.get("results").asJsonArray

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        var articleList = mutableListOf<Article>()
        for (i in 0..jsonResultsArray.size() - 1) {

            // Read in articleList attributes
            var article = (Gson().fromJson(jsonResultsArray.get(i), Article::class.java))
//
//            // "Media"
//            val mediaObject = jsonResultsArray.get(i).asJsonObject.get("media")
//                    .asJsonArray.get(0).asJsonObject
//
//            // "Media-Metadata"-Array
//            val mediametaArray = mediaObject.get("media-metadata").asJsonArray
//
//            // Read in all Article Images
//            val imageList = ArrayList()
//            for (j in 0..mediametaArray.size() - 1) {
//                val aritcleImage = Gson().fromJson<ArticleImage>(mediametaArray.get(j), ArticleImage::class.java!!)
//                imageList.add(aritcleImage)
//            }
//
            // Get the Publish Date
            val dateLine = jsonResultsArray.get(i).asJsonObject.get("published_date").asString
            article.date = simpleDateFormat.parse(dateLine)

//            // Set Article Image List
//            articleList.setURLImageList(imageList)
//
//            // add Article with all its attributes
//            articleList.add(articleList)
            articleList.add(article)
        }

        return articleList
    }
}

