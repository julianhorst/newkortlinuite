package com.trivago.newkortlinuite.network

import com.trivago.newkortlinuite.models.Article
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SearchApi {
    @GET("/svc/mostpopular/v2/mostviewed/{section}/{time-period}.json")
    fun getArticles(@Path("section") section: String,
                    @Path("time-period") time: String,
                    @Query("api-key") key: String)
            : Call<List<Article>>
}