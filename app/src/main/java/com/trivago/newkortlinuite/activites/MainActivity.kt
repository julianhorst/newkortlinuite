package com.trivago.newkortlinuite.activites

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import com.trivago.newkortlinuite.R
import com.trivago.newkortlinuite.adapter.ArticleAdapter
import com.trivago.newkortlinuite.models.Article
import com.trivago.newkortlinuite.network.OnResponseListener
import com.trivago.newkortlinuite.network.RestClient
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.selector

class MainActivity : AppCompatActivity(), OnResponseListener, AnkoLogger {

    // Members

    private lateinit var articleList: List<Article>

    // Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setup()
    }

    // Private Api

    private fun setup() {

        // Start initial network call

        val client = RestClient(this)
        client.start()

        // Setup click ding

        timeSpanButton.setOnClickListener {
            val spans = listOf("1", "7", "30")
            selector("Articles of how many days?", spans, { _, i ->
                val client2 = RestClient(this, spans[i])
                client2.start()
                searchField.text.clear()
                // TODO: update view
            })
        }

        // Setup

        searchButton.onClick {
            val list = mutableListOf<Article>()

            (0..articleList.size - 1)
                    .filter { articleList[it].abstract.contains(searchField.text) || articleList[it].title.contains(searchField.text) }
                    .forEach { list.add(articleList[it]) }

            val adapter = ArticleAdapter(list)
            recyclerView.adapter = adapter
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        val adapter = ArticleAdapter(articleList)
        recyclerView.adapter = adapter

        debug("Rhubarb")



    }

    // Interfaces

    override fun onResponse(articleList: List<Article>?) {
        if (articleList != null) this.articleList = articleList

        setupRecyclerView()
    }
}
